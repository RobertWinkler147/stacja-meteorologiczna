/*
 * DHT11.c
 *
 *  Created on: 09.06.2018
 *      Author: robert
 */

#include "DHT11.h"

void DHT11_Init(DHT11Struct* DHT11, TIM_HandleTypeDef* htim, uint32_t timOCChannel,
	            uint32_t timICChannel, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){

	DHT11->htim_ptr = htim;
	DHT11->timOCChannel = timOCChannel;
	DHT11->timICChannel = timICChannel;
	DHT11->GPIOx = GPIOx;
	DHT11->GPIO_Pin = GPIO_Pin;

	DHT11->TransmissionState = DHT11_TRANSMISSION_STATE_NONE;

	DHT11->ReadBuffer = 0;
	DHT11->ReadBufferIterator = 0;
	DHT11->TickCounter = 0;

	DHT11->EndOfTransmission_Callback = NULL;

	DHT11__ConfigurePinAsOutput_HIGH(DHT11);
}

void DHT11_SetEndOfTransmission_Callback(DHT11Struct* DHT11, void (*funPointer)()){
	DHT11->EndOfTransmission_Callback = funPointer;
}

void DHT11__SetInterval_IT(DHT11Struct* DHT11, uint32_t time_us) {
	uint32_t TimerCNTNext = __HAL_TIM_GET_COUNTER(DHT11->htim_ptr)
			+ time_us * 10;
	__HAL_TIM_SET_COMPARE(DHT11->htim_ptr, DHT11->timOCChannel, TimerCNTNext);
	HAL_TIM_OC_Start_IT(DHT11->htim_ptr, DHT11->timOCChannel);
}

void DHT11__InitializationProcedure(DHT11Struct* DHT11) {
	DHT11->TransmissionState = DHT11_TRANSMISSION_STATE_PULLUPLINE;
	DHT11__ConfigurePinAsOutput_LOW(DHT11);
	DHT11__SetInterval_IT(DHT11, 18000);
}

void DHT11_OC_DelayElapsedCallback_USE_IT_IN_CALLBACK(DHT11Struct* DHT11){
	if(DHT11->TransmissionState == DHT11_TRANSMISSION_STATE_PULLUPLINE){
		DHT11__ConfigurePinAsOutput_HIGH(DHT11);
		DHT11__SetInterval_IT(DHT11, 40);
		DHT11->ReadBuffer = 0;
		DHT11->ReadBufferIterator = 0;
		DHT11->TransmissionState = DHT11_TRANSMISSION_STATE_STARTREAD;
	}
	else if(DHT11->TransmissionState == DHT11_TRANSMISSION_STATE_STARTREAD){
		HAL_TIM_IC_Start_IT(DHT11->htim_ptr, DHT11->timICChannel);
		HAL_TIM_OC_Stop_IT(DHT11->htim_ptr, DHT11->timOCChannel);
	}
	else if(DHT11->TransmissionState == DHT11_TRANSMISSION_STATE_END){
		HAL_TIM_OC_Stop_IT(DHT11->htim_ptr, DHT11->timOCChannel);
		if(DHT11->EndOfTransmission_Callback != NULL) DHT11->EndOfTransmission_Callback();
	}
}

void DHT11__PutToReadBuffer(DHT11Struct* DHT11, uint8_t Bit){
	if(DHT11->ReadBufferIterator < 32){
		DHT11->ReadBuffer |= (Bit << (31 - DHT11->ReadBufferIterator));
		++DHT11->ReadBufferIterator;
	}
}

void DHT11_IC_CaptureCallback_USE_IT_IN_CALLBACK(DHT11Struct* DHT11){
	DHT11->TickCounter = __HAL_TIM_GET_COMPARE(DHT11->htim_ptr, DHT11->timICChannel);
	__HAL_TIM_SET_COUNTER(DHT11->htim_ptr , 0);
	if(DHT11->TickCounter >= 750 && DHT11->TickCounter <= 850){
	}
	else if(DHT11->TickCounter >= 450 && DHT11->TickCounter <= 550){
	}
	else if(DHT11->TickCounter >= 200 && DHT11->TickCounter <= 300){
		DHT11__PutToReadBuffer(DHT11, 0);
	}
	else if(DHT11->TickCounter >= 650 && DHT11->TickCounter <= 749){
		DHT11__PutToReadBuffer(DHT11, 1);
	}

	if(DHT11->ReadBufferIterator == 32){
		DHT11->TransmissionState = DHT11_TRANSMISSION_STATE_END;
		HAL_TIM_IC_Stop_IT(DHT11->htim_ptr, DHT11->timICChannel);
		DHT11__SetInterval_IT(DHT11, 10);
	}
}

void DHT11__ConfigurePinAsInput(DHT11Struct* DHT11) {
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = DHT11->GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;

	HAL_GPIO_Init(DHT11->GPIOx, &GPIO_InitStruct);
}

void DHT11__ConfigurePinAsOutput_HIGH(DHT11Struct* DHT11) {
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = DHT11->GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;

	HAL_GPIO_Init(DHT11->GPIOx, &GPIO_InitStruct);
}

void DHT11__ConfigurePinAsOutput_LOW(DHT11Struct* DHT11) {
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = DHT11->GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

	HAL_GPIO_Init(DHT11->GPIOx, &GPIO_InitStruct);
	HAL_GPIO_WritePin(DHT11->GPIOx, DHT11->GPIO_Pin, GPIO_PIN_RESET);
}

uint32_t DHT11_ReadHumidity(DHT11Struct* DHT11){
	float result = (float)(DHT11->ReadBuffer >> 24);
	uint16_t tmp = ((DHT11->ReadBuffer >> 16) & 0x000000FF);
	result += (float)(tmp / 128.0);
	return result*100;
}

uint32_t DHT11_ReadTemperature(DHT11Struct* DHT11){
	float result = (float)((DHT11->ReadBuffer >> 8) & 0x000000FF);
	uint16_t tmp = ((DHT11->ReadBuffer) & 0x000000FF);
	result += (float)(tmp / 128.0);
	return result*100;
}
